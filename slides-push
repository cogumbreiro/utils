#!/usr/bin/env python3
# 3rd-party
import pexpect
import yaml
# stdlib
import subprocess
import shlex
import shutil
import argparse
import os
import sys
import string

from pathlib import Path
from argparse import Namespace
from contextlib import contextmanager
# Type hints
from typing import Iterable, Iterator, ContextManager, Optional, List, Callable, Generator, Union

CURR_DIR = Path(__file__).parent.resolve()

class YamlConf:
    def __init__(self, file, more_hints=()):
        self.file = file
        self.more_hints = more_hints

    def load(self, hints=()):
        try:
            with open(self.file) as fp:
                self.conf = yaml.safe_load(fp)
        except FileNotFoundError:
            hint = f"create file '{self.file}'."
            self._abort("file does not exists!", hint, *hints)
            return

    def get(self, key, *hints):
        try:
            conf = self.conf
            sub_keys = []
            for sub_key in key.split("."):
                if conf is None:
                    path = ".".join(sub_keys)
                    if len(sub_keys) > 0:
                        self._abort(f"missing key '{sub_key}' under '{path}'", *hints)
                    else:
                        self._abort(f"missing key '{sub_key}'", *hints)
                conf = conf[sub_key]
                sub_keys.append(sub_key)
            return conf
        except KeyError:
            self._abort(f"missing key '{key}'", *hints)

    def try_get(self, key):
        conf = self.conf
        for sub_key in key.split("."):
            if conf is None:
                return
            conf = conf.get(sub_key, None)
        return conf

    def _abort(self, message, *hints):
        sys_abort(f"{self.file}: " + message, hints+self.more_hints)

def sys_abort(message:str, hints:Iterable[str]=()):
    print(message, file=sys.stderr)
    for hint in hints:
        print("Hint:", hint, file=sys.stderr)
    sys.exit(1)

def path_quote(p:Path) -> str:
    return shlex.quote(str(p))

class System:
    echo:bool
    dry_run:bool
    def __init__(self, args:Namespace):
        self.echo = args.echo
        self.output = args.output
        self.dry_run = args.dry_run
        self.environ = dict(os.environ)

    def resolve_patterns(self, patterns:List[str], base_dir:Optional[str]=None, **kwargs) -> List[Path]:
        paths:List[Path] = []
        for pattern in patterns:
            # First, evaluate variables
            pattern = self.template_subst(pattern, **kwargs)
            # Second, resolve relative paths
            if base_dir is not None and not os.path.isabs(pattern):
                pattern = os.path.join(base_dir, pattern)
            # Third, glob and convert to Path
            paths.extend(map(Path, glob.glob(pattern)))
        return paths

    def log(self, *args):
        if not self.echo:
            return
        result = []
        for elem in args:
            if isinstance(elem, Path):
                elem = path_quote(elem)
            result.append(elem)
        print(*result)

    def create_dir(self, path):
        # Only print if directory doesn't exist
        if not path.exists():
            self.log("mkdir -p", path)

        if not self.dry_run:
            path.mkdir(parents=True, exist_ok=True)

    def copy_file(self, src_file, dst_file, ignore_newer=False, ignore_missing=False):
        if not src_file.exists():
            sys_abort(f"{src_file}: error: file not found!")
            return
        copied = self.dry_run and src_file.is_file()
        if not self.dry_run and src_file.is_file() and (needs_update(src_file, dst_file) or ignore_newer):
            if not dst_file.parent.exists():
                self.create_dir(dst_file.parent)
            shutil.copyfile(src_file, dst_file)
            copied = True
        if copied:
            self.log("cp", src_file, dst_file)

    def copy_patterns(self, patterns, dst_dir, base_dir:Optional[str]=None):
        for src_file in self.resolve_patterns(patterns, base_dir=base_dir):
            self.copy_file(src_file, dst_dir / src_file.name)

    def copy_files(self, base_dir:Path, src_files:List[str], dst_dir:Path):
        for src_file in src_files:
            file = base_dir.joinpath(src_file)
            self.copy_file(file, dst_dir / file.name)

    def template_subst(self, tpl, **kwargs):
        env = dict(self.environ)
        for k, v in kwargs.items():
            env[k] = str(v)
        return string.Template(tpl).substitute(env)

    def call(self, cmd, echo=None, output=None):
        if echo is None:
            echo = self.echo
        if output is None:
            output = self.output

        if echo:
            if isinstance(cmd, str):
                print(cmd, file=sys.stderr)
            else:
                print(" ".join(cmd), file=sys.stderr)
        cmd_kwargs = {}
        if not output:
            cmd_kwargs["stdout"] = cmd_kwargs["stderr"] = subprocess.DEVNULL
        if self.dry_run:
            return True
        if isinstance(cmd, str):
            args = shlex.split(cmd)
        else:
            args = cmd
        result = subprocess.run(args=args, **cmd_kwargs)
        return result.returncode == 0

    def call_env(self, cmd:Union[str,List[str]], echo:Optional[bool]=None, output:Optional[bool]=None, **kwargs):
        def do_subst(x):
            return self.template_subst(x, **kwargs)
        if isinstance(cmd, str):
            cmd = do_subst(cmd)
        elif isinstance(cmd, list):
            cmd = list(map(do_subst, cmd))
        return self.call(cmd, echo=echo, output=output)


    @contextmanager
    def create_temporary_dir(self) -> Generator:
        """
        Creates a temporary directory.
        """
        if self.dry_run:
            # When running in dry_run do not create files, simply return tempdir
            work_dir = Path(tempfile.gettempdir())
            self.log("mkdir -p", work_dir)
            yield work_dir
            return

        with tempfile.TemporaryDirectory() as tmpdir:
            work_dir = Path(tmpdir)
            self.log("mkdir -p", work_dir)
            yield work_dir

def get_slide_count(fp):
    count = 1
    for line in fp:
        line = line.strip()
        if line == '---' or line == '--':
            count += 1
    return count

def export_html(env:System, slides:Path):
    env.call(["bs", "e", str(slides)])
    return Path("dist") / (slides.stem + ".html")

def export_pdf(env:System, slides:Path):
    pdf_file = Path("dist") / (slides.stem + ".pdf")
    url = "http://localhost:4100/" + slides.stem + ".html"
    # Get the slide count
    with open(slides) as fp:
        slide_count = get_slide_count(fp)
    if not env.dry_run:
        server = pexpect.spawn("bs s -s")
        server.expect("Watching files...")
        #print(slide_count)
        # Now we need to run pdf-export
        env.call(["node", str(CURR_DIR / "export-pdf.js"), url, str(slide_count), "-o", pdf_file])
        # Now we can close the server
        server.sendcontrol('c')
        server.close()
    return pdf_file

def needs_update(src:Path, dst:Path) -> bool:
    try:
        return dst.stat().st_mtime < src.stat().st_mtime
    except FileNotFoundError:
        return True

def export_slide_deck(file, args):
    # Get slides
    slides = Path(file)
    env = System(args)
    print("# Exporting file", file)
    conf = YamlConf(Path("slides-push.yaml"))
    conf.load()
    target_dir = Path(conf.get("target_dir", "target directory where the lecture files will be copied to."))
    target_pdf = target_dir / (slides.stem + ".pdf")
    target_html = target_dir / (slides.stem + ".html")
    # Export the HTML
    html_file = export_html(env, slides)
    env.copy_file(html_file, target_html)
    # Export PDF
    pdf_file = export_pdf(env, slides)
    env.copy_file(pdf_file, target_pdf)
    for to_copy_orig in conf.get("copy", ()):
        to_copy = env.template_subst(to_copy_orig,
            name=slides.name,
            stem=slides.stem,
            parent=slides.parent
        )
        print(f"# Copying: {to_copy} ({to_copy_orig})")
        to_copy_path = Path(to_copy)
        if to_copy_path.exists():
            env.copy_file(to_copy_path, target_dir / to_copy_path.name)
        else:
            print(f"# WARNING: File is missing: {to_copy_path}")

def main():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("files", nargs="*", help="Target slides we are exporting.")
    parser.add_argument("--echo", action="store_true",
                        help="Print commands.")
    parser.add_argument("--no-output", dest="output",
                        action="store_false", help="Print output of commands.")
    parser.add_argument("--dry-run", action="store_true",
                        help="Pretend to run the commands but don't run it.")
    args = parser.parse_args()
    for file in args.files:
        export_slide_deck(file, args)

if __name__ == '__main__':
    main()