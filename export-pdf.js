#!/usr/bin/env node
'use strict';
const puppeteer = require('puppeteer');
const path = require('path');
const cliProgress = require('cli-progress');
 
function run({url, slideCount, waitFor, target}) {
  (async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    const pathname = path.basename(new URL(url).pathname, ".html");
    const bar = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic);
    bar.start(slideCount, 0);

    await page.goto(url, {waitUntil:'networkidle0'});
    for (let i = 0; i < slideCount; i++) {
      bar.update(i + 1);
      //console.log('Writing slide #' + (1 + i));
      await page.keyboard.press('ArrowRight');
      await page.waitForTimeout(waitFor);
    }
    bar.stop();
    if (target == null) {
      target = pathname + '.pdf';
    }
    await page.pdf({
      path: target,
      margin: {top: 0, right: 0, bottom: 0, left: 0},
      width: 1024,
      height: 576});

    await browser.close();
  })();
}

var ArgumentParser = require('argparse').ArgumentParser;
var parser = new ArgumentParser({
  //version: '0.0.1',
  add_help:true,
  description: 'Generate PDF of lecture slides.'
});
parser.add_argument(
  'url',
  {
    help: 'The slide URL'
  }
);
parser.add_argument(
  'slideCount',
  {
    type: 'int',
    metavar: 'COUNT',
    help: 'The number of slides'
  }
);
parser.add_argument(
  '--wait-for', '-w',
  {
    type: 'int',
    dest: 'waitFor',
    metavar: 'MILISECS',
    default: 50,
    help: 'The time (ms) to wait per slide. Default: 50'
  }
);
parser.add_argument(
  '--output', '-o',
  {
    dest: 'target',
    help: 'Set a filename of the target PDF.'
  }
);
var args = parser.parse_args();
run(args);
